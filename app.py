from flask import Flask, render_template

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('login.html')

@app.route('/home',methods=['GET',"POST"])
def home():
    return render_template('index.html')

@app.route('/question')
def question():
    return render_template('question.html')

@app.route('/question/adapti')
def questionAdapti():
    return render_template('adapti.html')

# question/mostcom
@app.route('/question/mostcom')
def questionMostCommon():
    return render_template('mostcom.html')

@app.route('/test')
def test():
    return render_template('test.html')

@app.route('/demo')
def demo():
    return render_template('demo.html')



@app.route('/about')
def about():
    return render_template('about.html')

if __name__ == '__main__':
    app.run(debug=True)
